import React, { useState, useEffect } from "react"
import { Link, graphql } from "gatsby"

import SEO from "../components/seo"
import MainHeader from "../components/mainHeader"
import Layout from "../components/layout"
import Jumbotron from "../components/jumbotron"
import Skills from '../components/skills'
import Timeline from "../components/timeline"
import MiniCard from "../components/miniCard"
import Contacts from "../components/contacts"
import { DataPage } from "../components/data"

const IndexPage = ({ data }) => {
  const [showMore, setShowMore] = useState(false)
  const clickShowMore = () => {
    setShowMore(!showMore)
  }

  const project = data.allContentfulProjects.edges.filter(project => project.node.image.fluid !== null)

  const numberOfItems = showMore ? project.length : 3

  return (
    <Layout>
      <SEO title="Home" />
      <MainHeader />
      <section id="home" className="cover">
        <p className="lead">{DataPage.subhead}</p>
        <h1 className="cover-heading">{DataPage.head}</h1>
        <p className="lead mt-4">
          <Link to="/#about" class="btn btn-lg btn-secondary">
            About Me
          </Link>
        </p>
      </section>
      <section id="about" className="mb-5">
        <Jumbotron />
        <Skills />
        <Timeline />
      </section>
      <section id="projects" className="mb-5">
        <h2>My Several Projects:</h2>
        <div className="row justify-content-center">
          {project.slice(0, numberOfItems)
            .map(project => (
              <MiniCard
                key={project.node.id}
                title={project.node.title}
                image={project.node.image.fluid}
                desc={project.node.description.description}
                link={project.node.link}
              />
            ))}
        </div>
        <button className="btn btn-secondary mt-3" onClick={()=> clickShowMore()}>{showMore ? "Show less" : "Show all projects"}</button>
      </section>
      <section id="contacts">
        <Contacts />
      </section>
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query IndexQueryEn {
    allContentfulProjects(sort: { order: DESC, fields: createdAt }) {
      edges {
        node {
          id
          title
          description {
            id
            description
          }
          image {
            fluid {
              ...GatsbyContentfulFluid
            }
          }
          link
        }
      }
    }
  }
`
