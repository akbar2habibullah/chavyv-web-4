import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const MainHeader = () => (
  <header className="header mb-auto">
    <h3 className="header-brand">Chavyv.Akvar</h3>
    <nav className="nav nav-header justify-content-center">
      <Link className="nav-link" to="/#about">
        About
      </Link>
      <Link className="nav-link" to="/#projects">
        Projects
      </Link>
      <Link className="nav-link" to="/#contacts">
        Contacts
      </Link>
      <a className="nav-link" href="https://blog.chavyv.xyz/" target="_blank" rel="noreferrer">
        <i class="fas fa-paper-plane"></i>
      </a>
    </nav>
  </header>
)

MainHeader.propTypes = {
  siteTitle: PropTypes.string,
}

MainHeader.defaultProps = {
  siteTitle: ``,
}

export default MainHeader
