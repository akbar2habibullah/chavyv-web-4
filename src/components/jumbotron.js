import React from 'react'
import { Image, Container } from 'react-bootstrap'
import { DataPage } from './data'

export default function Jumbotron() {
    return (
        <div className="jumbotron mb-5">
            <Container>
                <Image src={DataPage.imgURL} className='mx-auto col-md-6 mb-3' alt="foto-Profil" roundedCircle />
                <h1 className="display-3">Hello, my name is <span className="highlight">Habib</span>ullah Akbar!</h1>
                <h3>but you can call me <span className="highlight">Habib</span></h3><br />
                {DataPage.desc.map(txt => (<p>{txt}</p>))}
                <a href={DataPage.resumeLink} rel="noreferrer" target="_blank" className="btn btn-secondary">Get my Resume</a>
            </Container>
        </div>
    )
}
