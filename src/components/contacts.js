import React from 'react'

export default function Contacts() {
    return (
        <>
            <h2>You can reach me with:</h2>
            <div className="contact-icon">
                <a href="mailto:akbar2habibulah@gmail.com" rel="noreferrer" target="_blank"><i class="far fa-envelope fa-3x"></i></a>
                <a href="https://instagram.com/chavyv.akvar" rel="noreferrer" target="_blank"><i class="fab fa-instagram fa-3x"></i></a>
                <a href="https://twitter.com/ChavyvAkvar" rel="noreferrer" target="_blank"><i class="fab fa-twitter fa-3x"></i></a>
                <a href="https://www.linkedin.com/in/habibullah-akbar-631880179/" rel="noreferrer" target="_blank"><i class="fab fa-linkedin-in fa-3x"></i></a>
                <a href="https://github.com/akbar2habibullah" rel="noreferrer" target="_blank"><i class="fab fa-github fa-3x"></i></a>
                <a href="https://gitlab.com/akbar2habibullah" rel="noreferrer" target="_blank"><i class="fab fa-gitlab fa-3x"></i></a>
            </div>
        </>
    )
}
