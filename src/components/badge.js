import React from 'react'

export default function Badge({title}) {
    return (
        <div className="btn badge mb-2">
            {title}
        </div>
    )
}
