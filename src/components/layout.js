/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { useState } from "react"
import PropTypes from "prop-types"

import Footer from './footer'

import "./layout.css"

const Layout = ({ children }) => {
  const [state, setState] = useState({
    darkMode: false
  })

  return (
    <>
      <main className={state.darkMode ? 'dark-mode text-center' : 'text-center'} style={{ width: '100vw' }}>
        <div className='cover-container d-flex w-100 h-100 p-3 mx-auto flex-column'>
          <div className="top-line"></div>
          <i id="dark-mode-btn" className={state.darkMode ? "toggler far fa-sun fa-2x mx-3 my-3" : "toggler far fa-moon fa-2x mx-3 my-3"} onClick={() => setState({ darkMode: !state.darkMode })}></i>
          {children}
          <Footer />
        </div>
      </main>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
