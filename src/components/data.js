import React from "react"

export const DataPage = {
  head: `Unique person, with all his question about the world`,
  subhead: `Kalem, idealis, suka begadang`,
  imgURL: `https://i.ibb.co/zNmDqs7/foto-Profil.jpg`,
  desc: [
    <>
      I am a Software Engineer from Indonesia. Having expertise in <span className="highlight">frontend engineering</span> 👨‍💻 and <span className="highlight">decentralised web</span> 🕸️. I have much experience as a remote frontend engineer at growing and mature startups. I also have solid experience working in some <span className="highlight">overseas companies</span> as a remote employee.
    </>,
    <>
      I am a person who is quick to learn things and able to work under
      pressure. I have a high level of interest and passion in the fields of{" "}
      <span className="highlight">science 👨‍🔬 and technology 🚀</span>.
      My life motto is "<span className="highlight">talk less 🤐, do more 💪</span>".
    </>,
  ],
  resumeLink: `https://drive.google.com/file/d/1Ov1aT7n4csNRiDYy-ZApmYercbCJPZ1F/view?usp=sharing`,
  timeline: [
    {
      time: "May 2013",
      badge: "Achievment",
      desc: "Got perfect score in elementary school mathematics national exam",
    },
    {
      time: "May 2016",
      badge: "Achievment",
      desc: "Got perfect score in junior high school mathematics national exam",
    },
    {
      time: "June 2016",
      badge: "Edu",
      desc: "Graduated from SMART Ekselensia Indonesia Junior High School ",
    },
    {
      time: "March 2017",
      badge: "Award",
      desc: "OSN Computer, 1st place at Bogor Regency level",
    },
    {
      time: "Oct 2017",
      badge: "Award",
      desc: "UIN Jakarta Physics Olympiad, 1st Place",
    },
    {
      time: "April 2018",
      badge: "Achievment",
      desc: "Got perfect score in senior high school mathematics national exam",
    },
    {
      time: "May 2018",
      badge: "Edu",
      desc:
        "Graduated from SMART Ekselensia Indonesia Senior High School in 2 years, Acceleration Program",
    },
    {
      time: "July 2018",
      badge: "Edu",
      desc:
        "Accepted as a student at the faculty of computer science, University of Indonesia",
    },
    {
      time: "Feb 2019 - Dec 2019",
      badge: "Volunteer",
      desc:
        "Volunteer as staff of propaganda and action department, BEM UI 2019",
    },
    {
      time: "March 2020 - May 2020",
      badge: "Volunteer",
      desc: "Volunteer as staff of research study department, BEM UI 2020",
    },
    {
      time: "Nov 2020 - Dec 2020",
      badge: "Work",
      desc: "Work as a social media strategist intern at Skydu Academy",
    },
    {
      time: "Jan 2021 - April 2021",
      badge: "Work",
      desc: "Work as a frontend developer intern at Pabryk",
    },
    {
      time: "May 2021 - Sept 2021",
      badge: "Work",
      desc: "Work as a frontend developer at Pabryk",
    },
    {
      time: "Oct 2021 - June 2022",
      badge: "Work",
      desc: "Work as a freelance frontend web developer at Warung Pintar",
    },
    {
      time: "March 2022 - June 2022",
      badge: "Work",
      desc: "Work as frontend engineer at HubbedIn",
    },
    {
      time: "August 2022 - Sep 2022",
      badge: "Work",
      desc: "Work as React developer at Stockifi.io",
    }
  ].reverse(),
}
