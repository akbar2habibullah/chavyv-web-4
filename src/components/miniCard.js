import React from 'react'
import Img from 'gatsby-image'

export default function MiniCard({ title, image, desc, link }) {
    return (
        <div className="card p-0 m-2 w-100">
            <div class="row no-gutters">
                <div className="col-md-8">
                    <Img fluid={image} className="card-img" />
                </div>
                <div className="col-md-4">
                    <div className="card-body">
                        <h5 className="card-title">{title}</h5>
                        <p className="card-text">{desc}</p>
                        <a href={link} className="btn btn-secondary" rel="noreferrer" target="_blank">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    )
}
