import React from "react"

export default function Footer() {
  return (
    <footer className="footer mt-5">
      <p>
        Made with ❤,{" "}
        <a href="https://getbootstrap.com/" rel="noreferrer" target="_blank">
          Bootstrap
        </a>{" "}
        &{" "}
        <a href="https://www.gatsbyjs.org/" rel="noreferrer" target="_blank">
          Gatsby
        </a>
        , Hosted in{" "}
        <a href="https://vercel.com/" rel="noreferrer" target="_blank">
          Vercel
        </a>
        , Source available on{" "}
        <a
          href="https://gitlab.com/akbar2habibullah/chavyv-web-4"
          rel="noreferrer"
          target="_blank"
        >
          GitLab
        </a>
        <br />
        Copyright &copy; {( new Date().getFullYear() )} Chavyv Akvar. All Rights Reserved
      </p>
    </footer>
  )
}
