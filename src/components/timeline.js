import React from "react"
import Badge from './badge'
import { DataPage } from './data'

export default function Timeline() {
  return (
    <>
      <h1 className="mb-4">My Timeline</h1>
      <div className="timeline">
        { DataPage.timeline.map(event => (
          <div className="container-timeline">
          <div className="content">
            <h2 className="highlight">{ event.time }</h2>
            <Badge title={ event.badge } />
            <p>
              { event.desc }
            </p>
          </div>
        </div>
        ))}
      </div>
    </>
  )
}
