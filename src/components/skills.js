import React from "react"

export default function Skills() {
  return (
    <>
      <h2 className="mb-4">My Technical Skills :</h2>

      <div class="container-bar">
        <div class="skills html">90%</div>
      </div>
      <p className="text-left mb-4">HTML / CSS</p>

      <div class="container-bar">
        <div class="skills javascript">95%</div>
      </div>
      <p className="text-left mb-4">Javascript / React / Vue / Angular</p>

      <div class="container-bar">
        <div class="skills node">80%</div>
      </div>
      <p className="text-left mb-4">NodeJS / Express</p>

      <div class="container-bar">
        <div class="skills python">90%</div>
      </div>
      <p className="text-left mb-4">Python / Django</p>

      <div class="container-bar">
        <div class="skills database">85%</div>
      </div>
      <p className="text-left mb-4">Database / PostgreSQL / MySQL / MongoDB</p>

      <div class="container-bar">
        <div class="skills java">80%</div>
      </div>
      <p className="text-left mb-4">Java / Data Structure / Algorithm</p>

      <div class="container-bar">
        <div class="skills design">85%</div>
      </div>
      <p className="text-left mb-4">Design Graphics</p>
    </>
  )
}
